<?php
require_once 'db.php';

if (isset ($_GET['slug'])) {
    $sql = "SELECT * FROM lection WHERE slug='{$_GET['slug']}'";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {

        $lectures = $result->fetch_all(MYSQLI_ASSOC);

        foreach ($lectures as $lecture) {
            ?>
            <ul>
                <li><?= $lecture["title"]; ?></li>
                <li><?= $lecture["content"] ?>;</li>
                <li><?= $lecture["data_create"] ?>;</li>
            </ul>

            <?php
            if (strlen($lecture["video_link"]) == 11) {
                ?>

                <iframe src="https://www.youtube.com/embed/<?= $lecture['video_link'] ?>" frameborder="0" allowfullscreen  width="100%" height="100%"></iframe>
            <?php
            } else {
                ?>
                <iframe src="https://player.vimeo.com/video/<?= $lecture["video_link"] ?>" frameborder="0" allowfullscreen  width="100%" height="100%"></iframe>

            <?php
            }
        }
    } else {
        header("Location: /");
        die;
    }
}
else {
    header("Location: /");
    die;
}
?>